import cafe.cryptography.curve25519.CompressedRistretto;
import org.whispersystems.curve25519.Curve25519;
import org.whispersystems.curve25519.Curve25519KeyPair;

public class Wallet {
    public static void main(String[] args) {
        Curve25519KeyPair keyPair = Curve25519.getInstance(Curve25519.BEST).generateKeyPair();
        CompressedRistretto expected = new CompressedRistretto(keyPair.getPublicKey());
        String address = Bech32.encode("stt", expected.toByteArray());
        System.out.println(address);
    }
}
